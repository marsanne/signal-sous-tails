#!/bin/bash

BOLD=$(tput bold)
STD=$(tput sgr0)

# Define Variables

SCRIPT_DIR=$1
DOTFILES=$2

# Create filesystem for installation
mkdir -pv /home/amnesia/Persistent/signal-conf/
mkdir -pv data/dynamic/signal-cli/version-check/
mkdir -pv data/dynamic/signal-desktop/
mkdir -pv $DOTFILES/.local/share/applications/
mkdir -pv /home/amnesia/.local/share/applications/
mkdir -pv $DOTFILES/Applications/signal-scripts/
mkdir -pv $DOTFILES/Applications/signal-desktop/
mkdir -pv /home/amnesia/Applications/
mkdir -pv $DOTFILES/.config/Signal/

# Copy files to system 
cp -v $SCRIPT_DIR/data/static/launchers/* $DOTFILES/.local/share/applications/
cp -v $SCRIPT_DIR/data/static/launchers/* /home/amnesia/.local/share/applications/

echo -e "Get signal-desktop keys"
cd data/dynamic/signal-desktop/
while true;do
	wget -T 15 -q -O - https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg && break
done
cd ../../../

# Add apt sources and install dependencies
echo -e "${BOLD}adding sources an installing dependencies, hit your password${STD}"
sudo ./scripts/sudo-install-commands.sh

## Download and install Signal-Desktop
if [ -f "data/dynamic/signal-desktop/*.deb" ]; 
	then
		echo -e "${BOLD}signal-desktop est déjà présent, pas besoin de le télécharger !${STD}"
	else
set -e

# Download signal
wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
sudo bash -c " cat signal-desktop-keyring.gpg | tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] tor+https://updates.signal.org/desktop/apt xenial main' |\
tee /etc/apt/sources.list.d/signal-xenial.list
apt update
apt install -y dialog
"

#Create a temporary file to store dialog result
tmp_file=$(tempfile 2>/dev/null) || tmp_file=/tmp/test$$
trap "rm -f $tmp_file" 0 1 2 5 15

# Menu for Signal multiple instances
dialog --menu "Vous pouvez avoir plusieurs instances de Signal sur votre ordinateur afin d'avoir plusieurs numéros en même temps. Ce ne prendra pas plus d'espace disque, et la mise à jour de l'un entrainera celle des autres. Combien d'instances Signal souhaitez vous installer ?"  20 60 12 1 "UNE instance Signal" 2 "DEUX instances Signal" 3 "TROIS instances Signal" 4 "QUATRE instances Signal" 2>$tmp_file
result=$(cat $tmp_file)

apt download signal-desktop
mkdir -p ~/Applications/signal-desktop
dpkg-deb -xv $(ls signal-desktop*.deb) ~/Applications/signal-desktop

# Create config directories
mkdir -p /live/persistence/TailsData_unlocked/dotfiles/.config/
mkdir -p /live/persistence/TailsData_unlocked/dotfiles/.local/share/applications

# Download Signal-template
template=/tmp/template.desktop
wget -nv https://0xacab.org/jc7v/signal-sous-tails/-/raw/master/code/launchers/Signal-template.desktop?inline=false -O $template

# Install as desktop icons as necessary
for ((i=1;i<=$result;i++ ))
do
    export SIGNALID=$i
    envsubst < $template > /home/amnesia/.local/share/applications/Signal-$i.desktop

done


#wget https://0xacab.org/jc7v/signal-sous-tails/-/raw/master/code/launchers/Signal.desktop?inline=false -O /home/amnesia/.local/share/applications/Signal.desktop


cp -r ~/Applications/ /live/persistence/TailsData_unlocked/dotfiles/
cp ~/.local/share/applications/*.desktop /live/persistence/TailsData_unlocked/dotfiles/.local/share/applications/

update-desktop-database /home/amnesia/.local/share/applications/

echo "Signal Desktop a été installé!"

# Move scripts to applications/signal-scripts
echo -e "${BOLD}Copie des scripts de Signal${STD}"
cp -v $SCRIPT_DIR/data/static/scripts/startup.sh $DOTFILES/Applications/signal-scripts/
cp -v $SCRIPT_DIR/data/static/scripts/new-conversation.py $DOTFILES/Applications/signal-scripts/

# Install Signal-cli
## Find Signal-cli latest version
wget -O ~/Tor\ Browser/signal-cli.tar.gz https://github.com/AsamK/signal-cli/releases/download/v0.11.11/signal-cli-0.11.11-Linux.tar.gz
tar -zxvf  ~/Tor\ Browser/signal-cli.tar.gz -C ~/Applications
cp -rv ~/Applications/signal-cli-0.11.11/ /live/persistence/TailsData_unlocked/dotfiles/Applications

cp -v ~/.bashrc $DOTFILES/
echo -e "export JAVA_TOOL_OPTIONS=\"-Djava.net.preferIPv4Stack=true\"\nalias signal-cli=\"torsocks ~/Applications/signal-cli-0.11.11/bin/signal-cli\"" >> ~/.bashrc
cp -v ~/.bashrc "$DOTFILES/"

# Pour l'update de .bashrc : https://unix.stackexchange.com/questions/438105/remove-certain-characters-in-a-text-file

# Let's configure the thing
echo -e "${BOLD}Voulez-vous configurer votre numéro signal maintenant ?(Y/n)${STD}"
read -p "(Par défaut Y)" -n 1 -r CONFIGUREYN
echo
if [[ ! $CONFIGUREYN =~ ^[Nn]$ ]]
then
	./scripts/configure.sh $DOTFILES
else 
	echo -e "${BOLD}Vous pourrez reprendre la configuration de votre numéro signal plus tard en executant depuis ce repertoire .scripts/configure.sh ${STD}"
	exit
fi

# Clean-up 

srm -rf data/dynamic/*
