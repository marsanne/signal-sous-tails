#!/bin/bash

BOLD=$(tput bold)
STD=$(tput sgr0)

# Define Variables

SCRIPT_DIR=$1
DOTFILES=$2

# Remove filesystem
srm -rf /home/amnesia/Persistent/signal-conf
srm -rf $DOTFILES/.local/share/applications/Signal*
srm -f /home/amnesia/.local/share/applications/Signal*
srm -rf $DOTFILES/Applications/signal-scripts
srm -rf $DOTFILES/Applications/signal-desktop
srm -rf /home/amnesia/Applications/signal-desktop
srm -rf $DOTFILES/.config/Signal
srm -rf /home/amnesia/Applications/signal-cli*
srm -rf $DOTFILES/Applications/signal-cli*

# echo -e "export JAVA_TOOL_OPTIONS=\"-Djava.net.preferIPv4Stack=true\"\nalias signal-cli=\"torsocks ~/Applications/signal-cli-$SCV0/bin/signal-cli\"" >> ~/.bashrc
# cp -v ~/.bashrc $DOTFILES/

# Pour l'update de .bashrc : https://unix.stackexchange.com/questions/438105/remove-certain-characters-in-a-text-file


# Clean-up 
srm -rf data/dynamic/*
