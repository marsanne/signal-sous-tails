#!/bin/bash

echo -e "adding the signal GPG key to APT keyring"
cat data/dynamic/signal-desktop/signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null

echo "Add the signal repo to APT"
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] tor+https://updates.signal.org/desktop/apt xenial main' | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list

echo "Updating packages list"
apt update
apt install -y openjdk-17-jdk



