#!/usr/bin/env bash

BOLD=$(tput bold)
STD=$(tput sgr0)

# Define Variables
DOTFILES=$2
touch /home/amnesia/Persistent/signal-conf/credentials
touch /home/amnesia/Persistent/signal-conf/captcha
touch /home/amnesia/Persistent/signal-conf/qrcode

# Get phone number

read -p "${BOLD}(Quel est votre numéro (sous la forme +xxxxxxxxxx)${STD}" -r NUMBER
echo -e "${BOLD}signal-cli et signal-desktop seront donc configurés pour le numéro $NUMBER. Votre numéro est aussi noté dans signal-conf dans votre dossier Persistent. Vous pouvez le stocker ou bon vous semble.${STD}"
echo "NUMBER :" > /home/amnesia/Persistent/signal-conf/credentials
echo $NUMBER > /home/amnesia/Persistent/signal-conf/credentials

# Version manuelle 
echo -e "${BOLD}Rendez vous depuis votre navigateur sur https://signalcaptchas.org/challenge/generate.html ou https://signalcaptchas.org/registration/generate.html et solutionnez le captcha${STD}"
echo 
echo -e "${BOLD}Dans le navigateur, ouvrir une console (clic droit sur la page, *inspecter*, puis *console* dans le bandeau des outils développeur. Résoudre le captcha. Dans la console, copier la longue suite de caractère qui s'affiche après signalcaptcha://${STD}"
echo -e "${BOLD}Collez là ici et appuyez sur entrée.${STD}"
read -p "${BOLD}(Quel est votre numéro (sous la forme +xxxxxxxxxx)${STD}" -r CAPTCHA
echo "CAPTCHA :" > /home/amnesia/Persistent/signal-conf/captcha
echo $CAPTCHA > /home/amnesia/Persistent/signal-conf/captcha

# Version automatisée 
# ouvrir un navigateur à la bonne URL
# ouvrir la console aussi
# grep de la phrase une fois le captcha fait

torsocks  $DOTFILES/Applications/signal-cli/bin/signal-cli -a $NUMBER register --captcha $CAPTCHA

echo -e "${BOLD}Vous allez maintenant recevoir un code au $NUMBER, entrez le ici et appuyez sur entrée.${STD}"
read -p "${BOLD}(Quel est votre code (sous la forme xxxxxx)${STD}" -r CODE
echo "CODE" > /home/amnesia/Persistent/signal-conf/code
echo $CODE > /home/amnesia/Persistent/signal-conf/code

torsocks $DOTFILES/Applications/signal-cli/bin/signal-cli -a $NUMBER verify $CODE

echo -e "${BOLD}Votre numero signal devrait être enregistré !${STD}"
echo -e "${BOLD}Il vous faut maintenant créer un NIP et le stocker dans un endroit de confiance. Ce script le copiera automatiquement dans signal-conf dans votre dossier Persistent.${STD}"
read -p "${BOLD}Quel est votre NIP ?${STD}" -r NIP
echo "NIP :" > /home/amnesia/Persistent/signal-conf/credentials
echo $NIP > /home/amnesia/Persistent/signal-conf/credentials

torsocks $DOTFILES/Applications/signal-cli/bin/signal-cli -a $NUMBER setPin $NIP

echo -e "${BOLD}Donnez un nom à ce compte signal et appuyez sur entrée${STD}" -r NAME
torsocks $DOTFILES/Applications/signal-cli/bin/signal-cli -a $NUMBER updateProfile --name $NAME
echo "NAME :" > /home/amnesia/Persistent/signal-conf/credentials
echo $NAME > /home/amnesia/Persistent/signal-conf/credentials 

# Save this signal-cli configuration
cp -rv /home/amnesia/.local/share/signal-cli/* cp /live/persistence/TailsData_unlocked/dotfiles/.local/share/signal-cli


echo -e "${BOLD}Lancement de signal-desktop${STD}"
nohup /live/persistence/TailsData_unlocked/dotfiles/Applications/signal-scripts/startup.sh >/dev/null 2>&1 &
echo
echo -e "${BOLD}Faites une capture d’écran en séléctionant uniquement la zone du QRCode. Utilisez pour cela l'outil de capture d'écran dans : Applications –› Utilitaires –› Capture d’écran Puis *Sélection* et *Prendre une capture d'écran*. (Si y'a un peu de blanc autour c'est pas grave !) ${STD}"
echo -e "${BOLD}Enregistrer la capture dans le dossier Images et la renommer qrcode.png ${STD}"
read -p "${BOLD}Puis revenez dans ce terminal et appuyez sur entrée pour continuer. ${STD}" -n 1 CONTINUE
echo
echo
QRCODE=$(zbarimg qrcode.png)
echo "QRCODE :" > /home/amnesia/Persistent/signal-conf/qrcode
echo $QRCODE > /home/amnesia/Persistent/signal-conf/qrcode

torsocks $DOTFILES/Applications/signal-cli/bin/signal-cli -a $NUMBER addDevice --uri "QRCODE" 

# Save signal-desktop configuration
       
cp -rv /home/amnesia/.config/Signal/* /live/persistence/TailsData_unlocked/dotfiles/.config/Signal

echo -e "${BOLD}Signal devrait apparaître dans votre menu d'Applications après un redemmarage de Tails.${STD}"


