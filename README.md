### Ce git n'est plus a jour###
https://0xacab.org/Ethel pour les mise a jour de ce projet est d'autre projets



# Signal sous Tails sans Smartphone

Le but de ce répertoire est double:

1. fournir de la doc sur comment installer/utiliser Signal sous Tails et plus particulièrement sans Smartphone
2. Fournir des scripts pour automatiser l'installation, les mises à jours, etc.

## Execution
Ouvrir le dossier, faire un clic droit dans le vide, et ouvrir dans un terminal. Puis tapez :
```console
./signal-tails.sh
```

## Paquet Debian

Il serait possible développer une application qui pourrait (dans l'idéal) faire partie de la distribution Debian et qui permettera une utlisation simple de Signal sans smartphones et plus particulièrement sous Tails. Son fonctionnement est décrit dans le dossier [debsignal](https://0xacab.org/jc7v/signal-sous-tails/tree/master/code/debsignal)

## Todo

* Faire tester le script par différentes personnes
* Fiabliliser le script d'installation, notamment en cas de bug du réseau. 
* Prevoir des sorties en fonction des erreurs
* Prevoir de purger en cas de bug du script au milieu de l'install
* Verifier l'existence d'une install precedente avant de lancer les commandes.
* Faire un outil de mise a jour qui verifie d'abord les versions en cours. Peut-être aussi verifier la version de JAVA necessaire et verifier si il faut le mettre à jour.

